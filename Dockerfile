ARG X_BASE_VERSION=stable
ARG REGISTRY=scidockreg.esac.esa.int:62510
FROM ${REGISTRY}/datalabs/x_base:${X_BASE_VERSION}-20.04
#FROM --platform=linux/amd64 ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

ENV GLOBAL_PROFILE_FILE=/etc/profile.d/cheops-sfc.sh
ENV RESOURCES_DIR=/resources

ENV MPS_VERSION=11.16.0
ENV MPS_PREFIX=/opt/mps
ENV MPS_RESOURCES=$RESOURCES_DIR/mps/$MPS_VERSION

ENV PG_USER=postgres
ENV PG_PREFIX=/usr/lib/postgresql/16
ENV PGDATA=/var/lib/postgresql/16/main

ENV JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
ENV JRE_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64/jre/

COPY root/ /

RUN set -euxv; \
    apt-get update; \
    apt-get install --no-install-recommends -y \
        openjdk-8-jdk \
        tint2; \
    chmod +x \
        /etc/sfcinit.sh \
        /etc/xstartup.sh \
        /init.d/00-init-writable-home.sh \
        /init.sh; \
    # Removes the programs' title bar and the minimize/resize/close buttons
    #sed -i '/^<applications>$/a  <application class="*">\n    <decor>no</decor>\n  </application>' /etc/xdg/openbox/rc.xml; \
    ############################################################################
    #
    # SFC environment
    #
    ############################################################################
    # Add variables to the global profile so that they are visible to the 
    # xstartup.sh script that will run as the non-root user
    echo "export PATH=$PG_PREFIX/bin:\$PATH" >> $GLOBAL_PROFILE_FILE; \
    echo "export JAVA_HOME=$JAVA_HOME" >> $GLOBAL_PROFILE_FILE; \
    echo "export JRE_HOME=$JRE_HOME" >> $GLOBAL_PROFILE_FILE; \
    echo "export MPS_VERSION=$MPS_VERSION" >> $GLOBAL_PROFILE_FILE; \
    echo "export MPS_PREFIX=$MPS_PREFIX" >> $GLOBAL_PROFILE_FILE; \
    echo "export MPS_RESOURCES=$MPS_RESOURCES" >> $GLOBAL_PROFILE_FILE; \
    ############################################################################
    # 
    # Postgres
    #
    ############################################################################
    apt-get install --no-install-recommends -y \
        postgresql-common -y \
        gnupg2; \
    # Add postgresql 16 to the apt repo
    /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh -y; \
    # install postgresql 16
    apt-get install --no-install-recommends \
        postgresql-16 -y \
        postgresql-client-16; \
    # 'pg_ctl start' will fail if /var/run/postgresql does not exist because
    # postgres writes its pid file into it.
    # Note: creating /var/run/postgresql does not work in datalabs, possibly
    # because /var/run is a pointer to /run. Creating /run/postgres works. 
    mkdir -p /run/postgresql; \
    chown $PG_USER:$PG_USER /run/postgresql; \
    # Delete the $PGDATA directory created by the postgres installation because
    # the config files are not correct
    rm -rf $PGDATA/*; \
    # Initialize the database
    su - $PG_USER -c "$PG_PREFIX/bin/initdb --auth trust -D $PGDATA"; \
    # Start the database
    su - $PG_USER -c "$PG_PREFIX/bin/pg_ctl start -D $PGDATA -l  $PGDATA/serverlog"; \
    # Set the password of the postgres user 
    su - $PG_USER -c "$PG_PREFIX/bin/psql -c \"ALTER USER $PG_USER PASSWORD 'postgres'\""; \
    # Create an empty MPS database
    su - $PG_USER -c "$PG_PREFIX/bin/createdb -U $PG_USER cheops_mps"; \
    # Import the database dump. Use a 'custom' format database dump as they are 
    # faster to import than sql dumps
    su - $PG_USER -c "$PG_PREFIX/bin/pg_restore -U $PG_USER -d cheops_mps --no-owner --no-privileges -j 4 $MPS_RESOURCES/cheops_mps_db_dump.custom"; \
    ############################################################################
    #
    # MPS
    #
    ############################################################################
    mkdir -p $MPS_PREFIX; \
    chmod -R go+rwx $MPS_PREFIX; \
    # Temporarily suppress exit-on-error as the installers return a non-zero 
    # exit code when successful.
    set +e; \
    yes | $MPS_RESOURCES/install_mps-server.bin; \
    yes | $MPS_RESOURCES/install_mps-hmi-fc.bin; \
    set -e; \
    cp $MPS_RESOURCES/mps.global.properties $MPS_PREFIX/mps-server-$MPS_VERSION/config/; \
    cp $MPS_RESOURCES/mps.global.properties $MPS_PREFIX/mps-hmi-fc-$MPS_VERSION/config/; \
    # Change permissions of all files to -rw-r--r--
    find $MPS_PREFIX -type f -print0 |xargs -0 chmod 644; \
    # Change permissions of all directories to drwxr-xr-x
    find $MPS_PREFIX -type d -print0 |xargs -0 chmod 755; \
    # Set the executable bit on the MPS server and sfc binaries
    chmod ugo+x $MPS_PREFIX/mps-server-$MPS_VERSION/bin/* $MPS_PREFIX/mps-hmi-fc-$MPS_VERSION/bin/* $MPS_PREFIX/mps-hmi-fc-$MPS_VERSION/lib/mps-hmi-fc-product/CHEOPS-SOC-MPS; \
    # Create and set file permissions on the directory where the MPS
    # writes its log file. The process will be owned by a non-root user and 
    # must be writable to all
    mkdir -p /tmp/cheops-mps; \
    chmod go+rwx /tmp/cheops-mps; \
    # Make MPS and SFC config directories writable to all to allow 
    # the non-root user to customize them
    chmod -R ugo+w /opt/mps/mps*/config; \
    ############################################################################
    #
    # Clean up
    #
    ############################################################################
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*;
 