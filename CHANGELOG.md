# Changelog

## 1.0.0 - 2025-03-05

Published on ESA Datalabs as CHEOPS-SFC 1.0.0-1 on 2025-03-02. Contains SFC release 11.16.0.
