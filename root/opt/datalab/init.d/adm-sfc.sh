 # This script will be be executed with root privileges at container start-up.

 # Start the postgres database
 su - $PG_USER -c "$PG_PREFIX/bin/pg_ctl start -D $PGDATA -l $PGDATA/serverlog"