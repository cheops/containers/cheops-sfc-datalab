#!/bin/bash

# This script sets the SFC input and output directories to point to directories  
# inside the user's home by modifying the MPS global config file and adds the
# exampe observation requests files into the SFC intray, then starts the MPS 
# server and the SFC GUI.

set -eux

# $HOME/my_workspace is the directory that is accessible from the "My Workspace"
# menu item in the datalabs page
SFC_WORK_DIR=$HOME/my_workspace/cheops-sfc

# Modify the MPS global properties files to place the SFC input and output
# trays, as well as log files, into a subdirectory of the user's home
sed -i "s#mps.input.tray.feasibility=.*#mps.input.tray.feasibility=$SFC_WORK_DIR/sfc-input#" $MPS_PREFIX/mps*/config/mps.global.properties
sed -i "s#mps.output.tray.sch_tool=.*#mps.output.tray.sch_tool=$SFC_WORK_DIR/sfc-output#" $MPS_PREFIX/mps*/config/mps.global.properties
sed -i "s#mps.log.client.location=.*#mps.log.client.location=$SFC_WORK_DIR/logs/client.log#" $MPS_PREFIX/mps*/config/mps.global.properties
sed -i "s#mps.log.server.location=.*#mps.log.server.location=$SFC_WORK_DIR/logs/server.log#" $MPS_PREFIX/mps*/config/mps.global.properties

# Create the SFC working directory in the user's home, if it does not already 
# exist
if [ ! -d $SFC_WORK_DIR ]; then
    mkdir -p $SFC_WORK_DIR
    mkdir -p $SFC_WORK_DIR/logs
    mkdir -p $SFC_WORK_DIR/sfc-input/templates
    mkdir -p $SFC_WORK_DIR/sfc-output
    cp $MPS_RESOURCES/CH*EXT_APP_ObservationRequests*xml $SFC_WORK_DIR/sfc-input/templates
fi

# Start the MPS server
$MPS_PREFIX/mps-server-$MPS_VERSION/bin/CHEOPS-SOC-MPS-SERVER &

# Start the SFC client
$MPS_PREFIX/mps-hmi-fc-$MPS_VERSION/bin/CHEOPS-SOC-MPS-FC
